<?php
  namespace xano\cli;

  class Option {
    private $name;
    private $type;
    private $usage;
    private $required;
    private $match;
    private $prefix;

    function __construct() {
      $this->type = "text";
      $this->required = false;
      $this->prefix = "-";
    }

    function name($name) {
      $this->name = $name;
      $this->match = "-".$name;
      return $this;
    }

    function getName() {
      return $this->name;
    }

    function getFullName() {
      return $this->prefix.$this->name;
    }

    function prefix($prefix) {
      $this->prefix = $prefix;
      return $this;
    }

    function getPrefix() {
      return $this->prefix;
    }

    function match($match) {
      $this->match = $match;
      return $this;
    }

    function getMatch() {
      return $this->match;
    }

    function type($type) {
      $this->type = $type;
      return $this;
    }

    function getType() {
      return $this->type;
    }

    function usage($usage) {
      $this->usage = $usage;
      return $this;
    }

    function getUsage() {
      return $this->usage;
    }

    function required() {
      $this->required = true;
      return $this;
    }

    function isRequired() {
      return $this->required;
    }
  }