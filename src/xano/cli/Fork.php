<?php
  namespace xano\cli;

  class Fork {
    private $pid;

    function __construct() {
      if (!function_exists('posix_getpid')) {
        throw new \Exception("Please configure your PHP instance with posix support.");
      }

      $this->pid = null;
    }

    function fork() {
      if (!function_exists('pcntl_fork')) {
        throw new \Exception("Please configure your PHP instance with pcntl support.");
      }

      $this->pid = pcntl_fork();
      if ($this->pid === -1) {
        throw new \Exception("unable to fork\n");
      }
    }

    function isParentProcess() {
      return $this->pid > 0;
    }

    function getForkedProcessId() {
      return $this->pid;
    }

    function getParentProcessId() {
      return posix_getppid();
    }

    function getCurrentProcessId() {
      return posix_getpid();
    }

    function isProcessAlive($pid) {
      $status = 0;
      $ret = pcntl_waitpid($pid, $status, WNOHANG);
      return $ret === 0;
    }

    function getCountAndRemoveDeadProcesses(&$pids) {
      $pidCount = 0;

      foreach($pids as $key => $val) {
        $ret = pcntl_waitpid($val, $status, WNOHANG);
        if ($ret === 0) {
          $pidCount++;
          continue;
        }

        $code = pcntl_wexitstatus($status);
        if ($code !== 0) {
          throw new \Exception("The process above returned a bad status code. Please check error logs.");
        }

        unset($pids[$key]);
      }

      return $pidCount;
    }

    function log($type) {
      $args = func_get_args();
      array_shift($args);

      $msg = call_user_func_array("sprintf", $args);

      $data = sprintf("%s - Task[%s]->proc[%s]: %s\n", date("m/d/y-H:i:s"), $type, $this->getCurrentProcessId(), $msg);
      echo $data;
    }

    private function makeTime($file) {
      $filename = pathinfo($file, PATHINFO_FILENAME);

      list(, $time) = explode("_", $filename);

      $year = substr($time, 0, 4);
      $month = substr($time, 4, 2);
      $day = substr($time, 6, 2);

      if ($month{0} == "0") $month = $month{1};
      if ($day{0} == "0") $day = $day{1};

      $hour = substr($time, 9, 2);
      $min = substr($time, 11, 2);
      $second = 0;

      return mktime($hour, $min, $second, $month, $day, $year);
    }
  }
