<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;
  use \xano\cli\Yaml as Yaml;

  class Build extends \xano\cli\Command {
    function getName() {
      return "build";
    }

    function getUsage() {
      return "create a local build - development or production";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("core")
          ->type("bool")
          ->usage("core platform only. no custom build extensions"),
        (new \xano\cli\Option())
          ->name("grep")
          ->type("text")
          ->usage("pattern to filter directories for a partial build"),
        (new \xano\cli\Option())
          ->name("p")
          ->type("bool")
          ->usage("production build - optimizations enabled"),
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $__START = microtime(true);

      $app->process("prepare", []);

      $find = System::getExecutablePath("find");
      $prepDir = sprintf("%s/xano_modules/_/", getcwd());
      $binDir = sprintf("%s/xano_modules/bin/", getcwd());
      $install = System::realpath(System::mergePaths($prepDir, "xano-install.sh"));
      if (file_exists($install)) {
        $yarnLock = System::realpath(System::mergePaths($prepDir, "yarn.lock"));
        $yarnLastLock = System::realpath(System::mergePaths($prepDir, ".last-yarn.lock"));
        if (@md5_file($yarnLock) !== @md5_file($yarnLastLock)) {
          $app->process("install", []);
          clearstatcache();
        }
      }

      $cfg = System::getSafeConfig();

      if (isset($params["p"])) {
        echo "******************\n";
        echo "*** PROD BUILD ***\n";
        echo "******************\n\n";
      } else {
        echo "*****************\n";
        echo "*** DEV BUILD ***\n";
        echo "*****************\n\n";
      }

      if (isset($cfg["env"]["build"])) {
        System::applyEnv($cfg["env"]["build"]);
      }

      $dirs = [];
      $dirs[] = escapeshellarg(System::realpath(System::mergePaths($prepDir, "extensions")));

      if (!isset($params["core"])) {
        $cmd = sprintf("%s %s -maxdepth 2 -type f 2>/dev/null | grep xano-build.sh",
          $find,
          implode(" ", $dirs)
        );

        if (($params["grep"] ?? false)) {
          $cmd .= sprintf(" | grep %s", escapeshellarg($params["grep"]));
          printf("filtering: %s\n", $params["grep"]);
        }

        $result = System::execute($cmd, null);

        $files = System::parseLines($result);

        $needToInstall = false;
        foreach($files as $file) {
          $fileDir = pathinfo($file, PATHINFO_DIRNAME);

          $yarnLock = System::realpath(System::mergePaths($fileDir, "yarn.lock"));
          $yarnLastLock = System::realpath(System::mergePaths($fileDir, ".last-yarn.lock"));
          if (@md5_file($yarnLock) !== @md5_file($yarnLastLock)) {
            $needToInstall = true;
          }
        }

        if ($needToInstall) {
          $app->process("install", $params);
        }

        $pids = [];
        $fork = new \xano\cli\Fork;

        $index = 0;
        foreach($files as $file) {
          $cmd = $file;
          if (!isset($params["p"])) {
            $cmd .= " dev";
          }

          $index++;

          $fork->fork();

          if ($fork->isParentProcess()) {
            $pids[] = $fork->getForkedProcessId();
            continue;
          }

          $__RUN_TIME = microtime(true);
          printf("running task[%d] in parallel: %s\n", $index, $cmd);
          try {
            System::execute($cmd);
          } catch(\Exception $e) {
            echo $e->getMessage()."\n";
            exit(1);
          }
          printf("task[%d]: %.2fs\n", $index, microtime(true) - $__RUN_TIME);
          exit(0);
        }

        while(true) {
          $ret = $fork->getCountAndRemoveDeadProcesses($pids);
          if (empty($ret)) break;
          usleep(100000);
        }
      }

      printf("packaging... one moment\n");

      $cmd = sprintf("%s %s -maxdepth 1 -type d 2>/dev/null | grep -e \"extensions/\"",
        $find,
        implode(" ", $dirs)
      );

      $result = System::execute($cmd);
      $files = System::parseLines($result);

      $rsync = System::getExecutablePath("rsync");
      $cmd = sprintf("%s -rlptDvK --inplace --delete ",
        $rsync
      );

      $cmd .= sprintf("--exclude=.* --exclude=xano-*.sh --exclude=loader.php --exclude=yarn* --exclude=package.json --exclude=webpack.config.js --exclude=/composer* --exclude=/vendor/ --exclude=/node_modules/ --exclude=/xano_modules/ ");

      $setupDir = System::realpath(System::mergePaths($prepDir, "setup"));
      if (file_exists($setupDir)) {
        $cmd .= sprintf("%s ", escapeshellarg($setupDir));
      }

      foreach($files as $file) {
        $cmd .= sprintf("%s ", escapeshellarg(System::realpath($file)."/"));
      }

      System::mkdir($binDir);

      $cmd .= $binDir;

      System::execute($cmd);

      $node_modules = sprintf("%s/node_modules", $prepDir);
      if (file_exists($node_modules)) {
        $cmd = sprintf("%s -rlptDvK --inplace --delete %s %s",
          $rsync,
          $node_modules,
          $binDir
        );
        System::execute($cmd);
      }

      $cmd = sprintf("%s %s %s",
        PHP_BINARY,
        escapeshellarg(sprintf("%s/xano_modules/bin/setup/install.php", getcwd())),
        escapeshellarg(sprintf("%s/xano_modules/bin/", getcwd()))
      );

      System::passthru($cmd);

      if (isset($cfg["devlicense"])) {
        $cfgDir = sprintf("%s/xano_modules/storage/cfg", getcwd());
        System::mkdir($cfgDir);
        $licFile = sprintf("%s/xano_modules/storage/cfg/license.config", getcwd());
        System::saveFile($licFile, System::json_encode($cfg["devlicense"], true));
      } elseif (isset($cfg["localcfg"])) {
        $licFile = sprintf("%s/xano_modules/bin/xano.yaml", getcwd());
        System::saveFile($licFile, Yaml::encode($cfg["localcfg"]));
      }

      printf("complete - total build time: %.2fs\n", microtime(true) - $__START);
    }
  }