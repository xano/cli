<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;
  use \xano\cli\Yaml as Yaml;

  class DockerPull extends \xano\cli\Command {
    function getName() {
      return "docker_pull";
    }

    function getUsage() {
      return "fetch latest docker images";
    }

    function isComposerEnabled() {
      return false;
    }

    function getOptions() {
      return [
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      $__START = microtime(true);

      $docker = System::getExecutablePath("docker");
      $cmd = sprintf("%s pull %s", $docker, escapeshellarg(Config::DOCKER_CLI));
      System::passthru($cmd);
    }
  }