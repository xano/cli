<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class Run extends \xano\cli\Command {
    function getName() {
      return "run";
    }

    function getUsage() {
      return "run local xano environment";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("core")
          ->type("bool")
          ->usage("core platform only. no custom build extensions"),
        (new \xano\cli\Option())
          ->name("grep")
          ->type("text")
          ->usage("pattern to filter directories for a partial build"),
        (new \xano\cli\Option())
          ->name("host")
          ->type("text")
          ->usage("allows you to use a host:port other than the default localhost:9000"),
        (new \xano\cli\Option())
          ->name("nobuild")
          ->type("bool")
          ->usage("skip build prior to run"),
        (new \xano\cli\Option())
          ->name("nobuild-once")
          ->type("bool")
          ->usage("skip build prior to run (only once)"),
        (new \xano\cli\Option())
          ->name("p")
          ->type("bool")
          ->usage("production build - optimizations enabled")
      ];
    }

    function onCleanup() {
      $this->cleanupTriggers(System::getRepos());

      if ($this->hasDocker()) {
        printf("stopping docker... one moment\n");
        $dockerCompose = System::getExecutablePath("docker-compose");
        $cmd = sprintf("%s -f %s --project-directory docker down", $dockerCompose, escapeshellarg("docker/docker-compose.yml"));
        System::passthru($cmd);
      }

      die("\nexiting...\n");
    }

    function hasDocker() {
      return is_dir("docker");
    }

    function run(\xano\cli\App $app, array $params) {
      pcntl_signal(SIGTERM, [$this, "onCleanup"]);
      pcntl_signal(SIGHUP,  [$this, "onCleanup"]);
      pcntl_signal(SIGUSR1, [$this, "onCleanup"]);
      pcntl_signal(SIGINT,  [$this, "onCleanup"]);

      $originalGrep = $params["grep"] ?? null;

      $first = true;
      while(true) {
        if (!isset($params["nobuild"]) && !isset($params["nobuild-once"])) {
          if ($first) printf("building first...\n\n");
          $args = [];
          if (isset($params["core"])) {
            $args[] = "-core";
          }
          if (isset($params["grep"])) {
            $args[] = "-grep";
            $args[] = $params["grep"];
          }
          if (isset($params["p"])) {
            $args[] = "-p";
          }
          $app->process("build", $args);
        } else {
          // printf("skipping build...\n");
          unset($params["nobuild-once"]);
        }

        $binDir = sprintf("%s/xano_modules/bin/", getcwd());
        if (!file_exists($binDir)) {
          throw new \Exception("No build present. A successful build is necessary for run.");
        }

        try {
          $watchman = System::getExecutablePath("watchman");
          if ($first) {
            // printf("watchman detected - updates will automatically restart environment\n");
          }
        } catch(\Exception $e) {
          // ignore - no watchman
          // printf("no watchman detected... updates require a manual restart for updates\n");
        }

        $killTag = System::uniqueId();

        if (isset($watchman)) {
          if (!file_exists(".watchmanconfig")) {
            throw new \Exception("Missing .watchmanconfig - Please create.");
          }

          $tmpFile = sprintf("%s/tmp_%s.json", getcwd(), System::uniqueId());

          try {
            $this->cleanupTriggers(System::getRepos());
            foreach(System::getRepos() as $repo) {
              $path = System::realpath($repo);
              $trigger = $this->createTrigger(Config::WATCHMAN_TRIGGER, $path, $killTag);
              System::saveFile($tmpFile, $trigger);

              $cmd = sprintf('%s -j > /dev/null < %s', $watchman, $tmpFile);
              System::execute($cmd);
            }
          } finally {
            System::unlink($tmpFile);
          }
        } 

        if ($this->hasDocker()) {
          if ($first) {
            printf("starting docker\n");
            $dockerCompose = System::getExecutablePath("docker-compose");
            $cmd = sprintf("%s -f %s --project-directory docker up -d", $dockerCompose, escapeshellarg("docker/docker-compose.yml"));

            System::passthru($cmd);

            $initScript = "docker/scripts/init.sh";
            if (file_exists($initScript)) {
              System::passthru($initScript);
            }
          }

          $host = "localhost:9000";
          printf("\n*** SUCCESS - local server: http://%s/\n", $host);
          $cmd = sprintf("%s %s %s",
            PHP_BINARY,  
            escapeshellarg(sprintf("%s/xano_modules/bin/sleep.php", getcwd())),
            Config::WATCHMAN_PREFIX.$killTag
          );
        } else {
          $host = $params["host"] ?? "localhost:9000";
          printf("\n*** SUCCESS - local server: http://%s/\n", $host);

          $cmd = sprintf("%s -S %s %s -- %s",
            PHP_BINARY,  
            escapeshellarg($host),
            escapeshellarg(sprintf("%s/xano_modules/bin/local-project.php", getcwd())),
            Config::WATCHMAN_PREFIX.$killTag
          );
        }

        $first = false;

        $ret = System::passthru($cmd, null);
        switch($ret) {
          case 15: {
            if (isset($watchman)) {
              printf("\n*** UPDATE DETECTED - REBUILDING\n\n");
              $this->cleanupTriggers(System::getRepos());
              try {
                $file = sprintf("%s/%s", sys_get_temp_dir(), Config::WATCHMAN_TRIGGER_FILE);
                $data = System::readFile($file);
                System::unlink($file);
                $ext = pathinfo($data, PATHINFO_EXTENSION);
                if (in_array($ext, ["php","yaml"])) {
                  $params["core"] = true;
                  unset($params["grep"]);
                } else {
                  $params["grep"] = $originalGrep;
                  unset($params["core"]);
                }
              } catch(\Exception $e) {
                // ignore
              }

              if (isset($params["nobuild"])) {
                printf("exiting due to -nobuild being set.\n");
                return;
              }

              continue 2;
            }

          } break;
        }
        var_dump(__LINE__);
        break;
      }
    }

    function cleanupTriggers($repos) {
      try {
        $watchman = System::getExecutablePath("watchman");

        foreach($repos as $repo) {
          $path = System::realpath($repo);

          $cmd = sprintf("%s watch-del %s",
            $watchman,
            escapeshellarg($path)
          );
          System::execute($cmd);
        }
      } catch(\Exception $e) {
        // no watchman
      }
    }

    function createTrigger($name, $dir, $killTag) {
      $cmd = System::getSelfCommand();
      $cmd[] = "watchman-trigger";
      $cmd[] = "-kill";
      $cmd[] = $killTag;

      $obj = [];
      $obj[] = "trigger";
      $obj[] = $dir;
      $obj[] = [
        "name" => $name,
        "expression" => [
          "anyof",
          ["match", "**/*", "wholename"]
        ],
        "append_files" => true,
        "command" => $cmd
      ];

      return json_encode($obj);
    }
  }