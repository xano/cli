<?php

  namespace xano\cli\command;

  use \xano\cli\Config as Config;
  use \xano\cli\System as System;

  class Run2 extends \xano\cli\Command {
    function getName() {
      return "run2";
    }

    function getUsage() {
      return "run local xano environment";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("core")
          ->type("bool")
          ->usage("core platform only. no custom build extensions"),
        (new \xano\cli\Option())
          ->name("sync-cmd")
          ->type("text")
          ->usage("custom command to run after changes are detected")
          ->required(),
        (new \xano\cli\Option())
          ->name("grep")
          ->type("text")
          ->usage("pattern to filter directories for a partial build"),
        (new \xano\cli\Option())
          ->name("port")
          ->type("text")
          ->usage("the port used to access web interface")
          ->required(),
        (new \xano\cli\Option())
          ->name("nobuild")
          ->type("bool")
          ->usage("skip build prior to run"),
        (new \xano\cli\Option())
          ->name("nobuild-once")
          ->type("bool")
          ->usage("skip build prior to run (only once)"),
        (new \xano\cli\Option())
          ->name("p")
          ->type("bool")
          ->usage("production build - optimizations enabled")
      ];
    }

    function onCleanup() {
      $this->cleanupTriggers($this->getRepos());

      die("\nexiting...\n");
    }

    function run(\xano\cli\App $app, array $params) {
      pcntl_signal(SIGTERM, [$this, "onCleanup"]);
      pcntl_signal(SIGHUP,  [$this, "onCleanup"]);
      pcntl_signal(SIGUSR1, [$this, "onCleanup"]);
      pcntl_signal(SIGINT,  [$this, "onCleanup"]);

      $originalGrep = $params["grep"] ?? null;

      $repos = $this->getRepos();

      $first = true;
      while(true) {
        if (!isset($params["nobuild"]) && !isset($params["nobuild-once"])) {
          if ($first) printf("building first...\n\n");
          $args = [];
          if (isset($params["core"])) {
            $args[] = "-core";
          }
          if (isset($params["grep"])) {
            $args[] = "-grep";
            $args[] = $params["grep"];
          }
          if (isset($params["p"])) {
            $args[] = "-p";
          }
          $app->process("build", $args);

          printf("build complete... syncing\n");
          System::passthru($params["sync-cmd"]);
        } else {
          // printf("skipping build...\n");
          unset($params["nobuild-once"]);
        }

        $binDir = sprintf("%s/xano_modules/bin/", getcwd());
        if (!file_exists($binDir)) {
          throw new \Exception("No build present. A successful build is necessary for run.");
        }

        $watchman = System::getExecutablePath("watchman");

        if (!file_exists(".watchmanconfig")) {
          throw new \Exception("Missing .watchmanconfig - Please create.");
        }

        $killTag = System::uniqueId();

        $tmpFile = sprintf("%s/tmp_%s.json", getcwd(), System::uniqueId());

        try {
          $this->cleanupTriggers($repos);
          foreach($repos as $repo) {
            $path = System::realpath($repo);
            $trigger = $this->createTrigger(Config::WATCHMAN_TRIGGER, $path, $killTag);
            System::saveFile($tmpFile, $trigger);

            $cmd = sprintf('%s -j > /dev/null < %s', $watchman, $tmpFile);
            System::execute($cmd);
          }
        } finally {
          System::unlink($tmpFile);
        }

        $host = "localhost";
        printf("\n*** SUCCESS - local server: http://%s:%s/\n", $host, $params["port"]);
        $cmd = sprintf("%s %s %s",
          PHP_BINARY,
          escapeshellarg(sprintf("%s/xano_modules/bin/sleep.php", getcwd())),
          Config::WATCHMAN_PREFIX.$killTag
        );

        $first = false;

        $ret = System::passthru($cmd, null);
        switch($ret) {
          case 15: {
            if (isset($watchman)) {
              printf("\n*** UPDATE DETECTED - REBUILDING\n\n");
              $this->cleanupTriggers($repos);
              try {
                $file = sprintf("%s/%s", sys_get_temp_dir(), Config::WATCHMAN_TRIGGER_FILE);
                $data = System::readFile($file);
                System::unlink($file);
                $ext = pathinfo($data, PATHINFO_EXTENSION);
                if (in_array($ext, ["php","yaml"])) {
                  $params["core"] = true;
                  unset($params["grep"]);
                } else {
                  $params["grep"] = $originalGrep;
                  unset($params["core"]);
                }
              } catch(\Exception $e) {
                // ignore
              }

              if (isset($params["nobuild"])) {
                printf("exiting due to -nobuild being set.\n");
                return;
              }

              continue 2;
            }

          } break;
        }
        var_dump(__LINE__);
        break;
      }
    }

    function cleanupTriggers($repos) {
      try {
        $watchman = System::getExecutablePath("watchman");

        foreach($repos as $repo) {
          $path = System::realpath($repo);

          $cmd = sprintf("%s watch-del %s",
            $watchman,
            escapeshellarg($path)
          );
          System::execute($cmd);
          break;
        }
      } catch(\Exception $e) {
        // no watchman
      }
    }

    function getRepos() {
      $repos = System::getRepos();

      return [$repos[0]];
    }

    function createTrigger($name, $dir, $killTag) {
      $cmd = System::getSelfCommand();
      $cmd[] = "watchman-trigger";
      $cmd[] = "-kill";
      $cmd[] = $killTag;

      $obj = [];
      $obj[] = "trigger";
      $obj[] = $dir;
      $obj[] = [
        "name" => $name,
        "expression" => [
          "anyof",
          ["match", "**/*", "wholename"]
        ],
        "append_files" => true,
        "command" => $cmd
      ];

      return json_encode($obj);
    }
  }