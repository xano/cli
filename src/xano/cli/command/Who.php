<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Who extends \xano\cli\Command {
    function getName() {
      return "who";
    }

    function getUsage() {
      return "display the current user";
    }

    function isComposerEnabled() {
      return false;
    }

    function getOptions() {
      return [];
    }

    function run(\xano\cli\App $app, array $params) {
      printf("%s\n", \xano\cli\System::getUser());
    }
  }