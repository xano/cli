<?php

  namespace xano\cli\command;

  use \xano\cli\System as System;

  class Prepare extends \xano\cli\Command {
    function isHidden() {
      return true;
    }

    function getName() {
      return "prepare";
    }

    function getUsage() {
      return "prepare repos for build";
    }

    function getOptions() {
      return [
        (new \xano\cli\Option())
          ->name("fresh")
          ->type("bool")
          ->usage("delete generated files"),
      ];
    }

    function run(\xano\cli\App $app, array $params) {
      printf("initializing...\n");

      $prepDir = sprintf("%s/xano_modules/_/", getcwd());

      $rsync = System::getExecutablePath("rsync");

      $dirs = [];
      $repos = System::getRepos();
      foreach($repos as $repo) {
        $dirs[] = escapeshellarg(System::realpath(System::mergePaths($repo, "extensions")));

        foreach(["setup","package.json","yarn.lock","xano-build.sh","xano-install.sh","xano-upgrade.sh"] as $file) {
          $setupDir = System::realpath(System::mergePaths($repo, $file));
          if (file_exists($setupDir)) {
            $dirs[] = escapeshellarg($setupDir);
          }
        }
      }

      System::mkdir($prepDir);
      
      $options = [
        "-rlptDvK",
        "--inplace",
        "--exclude=node_modules/",
      ];

      if ($params["fresh"] ?? false) {
        $options[] = "--delete";
      } else {
        $options[] = "--exclude=xano_modules/";
        // foreach($repos as $repo) {
        //   if (strpos($repo, "xano_modules/x2") !== FALSE) {
        //     $options[] = "--include=xano_modules/x2";
        //   }
        // }
      }

      $cmd = sprintf("%s %s %s %s",
        $rsync,
        implode(" ", $options),
        implode(" ", $dirs),
        escapeshellarg($prepDir)
      );

      System::execute($cmd);
    }
  }