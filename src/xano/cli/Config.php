<?php
  namespace xano\cli;

  class Config {
    const VERSION = "2.87";
    const WATCHMAN_PREFIX = "xano-";
    const WATCHMAN_TRIGGER = "xanorun";
    const COMPOSER_VENDOR_PATH = "xano_modules/bin/vendor";
    const WATCHMAN_TRIGGER_FILE = ".watchman-triggerfile.log";
    const DOCKER_CLI = "registry.gitlab.com/xano/cli:latest";
    // const DOCKER_CLI = "cli-local:latest";
  }