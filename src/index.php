<?php
  init();

  try {
    $app = new \xano\cli\App(array_slice($_SERVER["argv"], 1));

    $app->scan();
    $app->run();

  } catch(\Exception $e) {
    printf("Error: %s\n\n", $e->getMessage());
    exit(1);
  }

  function init() {
    ini_set("display_errors", "on");
    $paths = explode(":", get_include_path());
    $paths[] = ".";

    if (isset($_SERVER["XANO_INCLUDE_PATH"])) {
      foreach(explode(":", $_SERVER["XANO_INCLUDE_PATH"]) as $path) {
        $path = trim($path);
        if (!empty($path)) {
          $paths[] = $path;
        }
      }
    }

    if (getcwd() != __DIR__) {
      array_unshift($paths, __DIR__); // this is needed for development
    }

    set_include_path(implode(":", array_unique($paths)));

    spl_autoload_register(function($classname) {
      $file = str_replace("\\", "/", $classname).".php";
      include_once $file;
    });
  };

