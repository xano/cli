FROM alpine:3.9 AS runtime

# Step 2: Install watchman runtime dependencies:
RUN apk add --no-cache libcrypto1.1 libgcc libstdc++

# Stage II: Builder ============================================================

# Step 3: Begin from runtime stage image:
FROM runtime AS builder

# Step 4: Install the watchman build dependencies' dependencies :)
RUN apk add --no-cache --update ca-certificates openssl

# Step 5: Install the watchman build dependencies:
RUN apk add --no-cache \
 automake \
 autoconf \
 bash \
 build-base \
 libtool \
 linux-headers \
 openssl-dev \
 python-dev

# Step 6: Set environment variables:
ENV WATCHMAN_VERSION=4.9.0 \
    WATCHMAN_SHA256=1f6402dc70b1d056fffc3748f2fdcecff730d8843bb6936de395b3443ce05322

# Step 7: Download watchman source code:
RUN cd /tmp \
 && wget -O watchman.tar.gz "https://github.com/facebook/watchman/archive/v${WATCHMAN_VERSION}.tar.gz" \
 && echo "$WATCHMAN_SHA256 *watchman.tar.gz" | sha256sum -c - \
 && tar -xz -f watchman.tar.gz -C /tmp/ \
 && rm -rf watchman.tar.gz

# Step 8: Build watchman from source:
RUN cd /tmp/watchman-${WATCHMAN_VERSION} \
 && ./autogen.sh \
 && ./configure --enable-lenient \
 && make \
 && make install \
 && cd $HOME \
 && rm -rf /tmp/*

FROM alpine:3.9 AS gcloud

RUN apk add --no-cache bash curl python-dev

RUN curl -sSL https://sdk.cloud.google.com | bash

# Stage III: Release ===========================================================

FROM registry.gitlab.com/xano/docker/xano-php:latest

RUN apk add --no-cache yarn git openssh docker py-pip python-dev libffi-dev openssl-dev gcc libc-dev make

RUN pip install docker-compose

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl

RUN curl -Lo ./kind https://github.com/kubernetes-sigs/kind/releases/download/v0.5.1/kind-$(uname)-amd64 && chmod +x ./kind && mv ./kind /usr/local/bin/kind

# RUN apk add --no-cache libcrypto1.1 libgcc libstdc++
# RUN apk add --no-cache --update ca-certificates openssl

# Step 10: Copy the compiled executable:
COPY --from=builder /usr/local/bin/watchman* /usr/local/bin/

# Step 12: Copy the runtime directories:
COPY --from=builder /usr/local/var/run/watchman /usr/local/var/run/watchman

COPY --from=gcloud /root/google-cloud-sdk/ /google-cloud-sdk/

ENV PATH /google-cloud-sdk/bin:$PATH

RUN mkdir /cli

COPY ./dist/setup-xanocli.php /cli
COPY ./dist/xanocli.tar.gz /cli/local-xanocli.tar.gz

RUN apk add inotify-tools

RUN php /cli/setup-xanocli.php --install-dir=/usr/local/bin --filename=xano --archive=/cli/local-xanocli.tar.gz && xano version && rm -f /cli/*

CMD tail -f /dev/null