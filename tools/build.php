<?php
  require_once __DIR__."/../src/xano/cli/System.php";

  use \xano\cli\System as System;

  try {
    printf("Building xano cli...\n\n");

    $dst = __DIR__."/../dist/xanocli.tar.gz";

    $cfgFile = __DIR__."/../src/xano/cli/Config.php";

    $cfg = System::readFile($cfgFile);
    $ret = @preg_match("#VERSION\s*=\s*\"(\d+\.\d+)\"#", $cfg, $matches);
    if (!isset($matches[1])) {
      throw new \Exception("Unable to retrieve version.");
    }

    $from = $matches[0];
    $to = str_replace($matches[1], sprintf("%.2f", $matches[1]+0.01), $from);

    printf("updated version to %s\n", $to);

    $cfg = str_replace($from, $to, $cfg);
    System::saveFile($cfgFile, $cfg);

    printf("creating archive\n");

    $cmd = sprintf("tar -zcvf %s -C %s %s %s",
      escapeshellarg($dst),
      escapeshellarg(__DIR__."/../src"),
      "index.php",
      "xano"
    );

    @unlink($dst);
    System::execute($cmd);
  } catch(\Exception $e) {
    printf("Error: %s\n", $e->getMessage());
    exit(1);
  }

  echo "\ndone\n";
