Xano CLI - Command line interface to manage Xano
================================================

Xano CLI helps you build, install, sync, run, and manage your Xano environment.

Standard Installation
------------

```
curl -s "https://gitlab.com/xano/cli/raw/master/dist/setup-xanocli.php" | sudo php
```

> NOTE: sudo is necessary because files get installed in your system's php include_path.


Installer Options
-----------------

- --install-dir - choose a custom directory to install xano cli. (default: /usr/local/bin)
- --filename - choose a custom name for the xano cli. (default: xano)

Custom Installation
------------
Here is the standard installation method using the install options with their default values. If customization is needed, then these options can be adjusted to whatever makes sense for your requirements.

```
curl -s "https://gitlab.com/xano/cli/raw/master/dist/setup-xanocli.php" | sudo php -- --install-dir=/usr/local/bin --filename=xano
```
NOTE: the "--" right after php is necessary. It signals to php that the options following it are intended for the setup script and not the php binary.


Requirements
------------

Xano requires PHP 7.1 or above.